FROM node:alpine as builder
WORKDIR '/app'
COPY . .

FROM nginx
EXPOSE 80
COPY --from=builder /app /usr/share/nginx/html